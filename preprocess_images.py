import loader

img_path = 'data/cube_plus/'
dest_path = 'data/cube_plus_processed/'
img_suffix = ".PNG"
num_of_examples = 1707
dims = (500, 500)

loader.load_and_save_images(img_path, img_suffix, num_of_examples, dims,dest_path)


