import numpy as np
import dataset
import errors


def trimean(errors):
    first_quartil = np.percentile(errors, 25)
    third_quartil = np.percentile(errors, 75)

    return (first_quartil + 2 * np.median(errors) + third_quartil) / 4


def get_errors(y_pred, y_true, error_function):
    errors = []

    for i in range(0, len(y_pred)):
        errors.append(degree(error_function(y_pred[i], y_true[i])))

    return np.array(errors)


def print_errors(predictions, y_true, error_function, error_name="angular"):
    calc_errors = get_errors(predictions, y_true, error_function)

    min_value = np.min(calc_errors)
    q1_value = np.percentile(calc_errors, 25)
    q3_value = np.percentile(calc_errors, 75)
    median = np.median(calc_errors)
    mean = np.mean(calc_errors)
    trimean_value = trimean(calc_errors)
    max_value = np.max(calc_errors)

    print("Min " + error_name + " error: ", min_value)
    print("First quartille " + error_name + " error: ", q1_value)
    print("Median " + error_name + " error: ", median)
    print("Mean " + error_name + " error: ", mean)
    print("Trimean " + error_name + " error: ", trimean_value)
    print("Third quartille " + error_name + " error: ", q3_value)
    print("Max " + error_name + " error: ", max_value)

    print("LATEX OUTPUT: ")
    print(" & " + str(round(float(mean), 2)) + " & " + str(round(float(median), 2)) + " & " + str(
        round(float(min_value), 2)) + " & " + str(round(float(q1_value), 2)) + " & " + str(
        round(float(q3_value), 2)) + " & " + str(round(float(max_value), 2)) + " & " + str(
        round(float(trimean_value), 2)))


def save_output(fname, predictions):
    np.savetxt("logs/predictions/" + fname, predictions, fmt="%1.8f")


def unfreeze(layer):
    layer.trainable = True
    return


def freeze(layer):
    layer.trainable = False
    return


def test_model(model, predictions_path):
    x_test, y_test = dataset.load_test_display()
    predictions = model.predict(x_test, batch_size=16)
    save_output(predictions_path, predictions)
    print("Angular errors: ")
    print_errors(predictions, y_test, errors.angular_error)
    print("Reproduction errors: ")
    print_errors(predictions, y_test, errors.reproduction_error, error_name="reproduction")



def test_model_validation(model, predictions_path):
    x_test, y_test = dataset.load_validation_cube()
    predictions = model.predict(x_test, batch_size=16)
    save_output(predictions_path, predictions)
    print("Angular errors: ")
    print_errors(predictions, y_test, errors.angular_error)
    print("Reproduction errors: ")
    print_errors(predictions, y_test, errors.reproduction_error, error_name="reproduction")


def degree(rad):
    return (rad * 180) / np.pi
