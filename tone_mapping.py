import numpy as np


def perform_flash(source, a=5, target=-1, perform_gamma_correction=True):
    rows, cols, _ = source.shape

    v = np.max(source, axis=2)
    vd = np.copy(v)
    vd[vd == 0] = 1e-9
    result = source / (a * np.exp(np.mean(np.log(vd))) + np.tile(np.expand_dims(vd, axis=2), (1, 1, 3)))

    if perform_gamma_correction:
        result **= 1.0 / 2.2

    if target >= 0:
        result *= target / np.mean((0.299 * result[:, :, 2] + 0.587 * result[:, :, 1] + 0.114 * result[:, :, 0]))
    else:
        result *= 255.0 / np.max(result)

    return result
