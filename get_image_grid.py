import matplotlib.pyplot as plt
import loader
import numpy as np
from tone_mapping import perform_flash

label_path = "data/cube+_gt.txt"
load_model_name = "model_ex1_best.h5"
load_model_path = 'data/models/'
img_path = 'data/cube_plus/'
img_suffix = ".PNG"
num_of_examples = 363
dims = (1000, 1000)
imgs = []
img_nums = [150, 552, 365]
labels = loader.load_labels(label_path, 1707)


def transform_vector(vector):
    return np.array([vector[2], vector[1], vector[0]])


def correct_image(img, vector):
    corr_im = img / vector[None, None, :]
    img = (corr_im / np.max(corr_im))
    return tone_map(img)


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


for img_num in img_nums:
    imgs.append(loader.load_image(img_path + str(img_num) + img_suffix, dims))

f, axarr = plt.subplots(len(imgs), 3)
for i, img in enumerate(imgs):
    num = img_nums[i] - 1
    axarr[i, 0].imshow(img)
    axarr[i, 1].imshow(tone_map(img))
    axarr[i, 2].imshow(correct_image(img, labels[num]))
    axarr[i, 0].axis("off")
    axarr[i, 1].axis("off")
    axarr[i, 2].axis("off")

axarr[2, 0].text(0.5, -0.15, "a)", size=12, ha="center", transform=axarr[2, 0].transAxes)
axarr[2, 1].text(0.5, -0.15, "b)", size=12, ha="center", transform=axarr[2, 1].transAxes)
axarr[2, 2].text(0.5, -0.15, "c)", size=12, ha="center", transform=axarr[2, 2].transAxes)
f.tight_layout()
plt.show()
