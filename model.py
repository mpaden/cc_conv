import tensorflow.keras as tk
from layers import SumLayer
from layers import ConfidenceLayer
from errors import tf_mean_angular_error


def load_model(path):
    return tk.models.load_model(path, custom_objects={'ConfidenceLayer': ConfidenceLayer,
                                                      'SumLayer': SumLayer,
                                                      'tf_mean_angular_error': tf_mean_angular_error})


def build_VGG16_basic():
    base_model = tk.applications.VGG16(weights='imagenet', include_top=False)
    base_model.trainable = False
    model = tk.Sequential(name="fc4vgg")

    model.add(base_model)

    model.add(tk.layers.Conv2D(filters=64, kernel_size=(6, 6), activation="relu", kernel_initializer="random_uniform",
                               name="fc4_conv1"))
    model.add(tk.layers.Conv2D(filters=4, kernel_size=(1, 1), activation="relu", kernel_initializer="random_uniform",
                               bias_initializer="ones",
                               name="fc4_conv2"))
    model.add(ConfidenceLayer(name="fc4_conf"))
    model.add(SumLayer(name="fc4_sum"))
    return model


def build_VGG16_no_confidence():
    base_model = tk.applications.VGG16(weights='imagenet', include_top=False)
    base_model.trainable = False
    model = tk.Sequential(name="fc4vgg")

    model.add(base_model)

    model.add(tk.layers.Conv2D(filters=64, kernel_size=(6, 6), activation="relu", kernel_initializer="random_uniform",
                               name="fc4_conv1"))
    model.add(tk.layers.Conv2D(filters=3, kernel_size=(1, 1), activation="relu", kernel_initializer="random_uniform",
                               bias_initializer="ones",
                               name="fc4_conv2"))
    model.add(SumLayer(name="fc4_sum"))
    return model


def build_VGG16_minimum():
    base_model = tk.applications.VGG16(weights='imagenet', include_top=False)
    base_model.trainable = False
    model = tk.Sequential(name="fc4vgg")

    model.add(base_model)

    model.add(tk.layers.Conv2D(filters=16, kernel_size=(4, 4), activation="relu", kernel_initializer="random_uniform",
                               name="fc4_conv1"))
    model.add(tk.layers.Conv2D(filters=4, kernel_size=(1, 1), activation="relu", kernel_initializer="random_uniform",
                               bias_initializer="ones",
                               name="fc4_conv2"))
    model.add(ConfidenceLayer(name="fc4_conf"))
    model.add(SumLayer(name="fc4_sum"))
    return model


def build_mobile_net_basic():
    base_model = tk.applications.mobilenet.MobileNet(input_shape=(500, 500, 3), alpha=1.0, depth_multiplier=1,
                                                     dropout=1e-3, include_top=False, weights='imagenet')
    base_model.summary()
    base_model.trainable = False
    model = tk.Sequential(name="fc4vgg")
    model.add(base_model)

    model.add(tk.layers.Conv2D(filters=64, kernel_size=(6, 6), activation="relu", kernel_initializer="random_uniform",
                               name="fc4_conv1"))
    model.add(tk.layers.Conv2D(filters=4, kernel_size=(1, 1), activation="relu", kernel_initializer="random_uniform",
                               bias_initializer="random_uniform",
                               name="fc4_conv2"))
    model.add(ConfidenceLayer(name="fc4_conf"))
    model.add(SumLayer(name="fc4_sum"))
    return model


def build_xception():
    base_model = tk.applications.xception.Xception(input_shape=(500, 500, 3),
                                                   include_top=False, weights='imagenet')
    base_model.summary()
    base_model.trainable = False
    model = tk.Sequential(name="fc4vgg")
    model.add(base_model)

    model.add(tk.layers.Conv2D(filters=64, kernel_size=(6, 6), activation="relu", kernel_initializer="random_uniform",
                               name="fc4_conv1"))
    model.add(tk.layers.Conv2D(filters=4, kernel_size=(1, 1), activation="relu", kernel_initializer="random_uniform",
                               bias_initializer="random_uniform",
                               name="fc4_conv2"))
    model.add(ConfidenceLayer(name="fc4_conf"))
    model.add(SumLayer(name="fc4_sum"))
    return model
