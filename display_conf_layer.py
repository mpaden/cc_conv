from tensorflow.keras.models import Model
import numpy as np
import cv2
from model import load_model
import dataset
from tone_mapping import perform_flash

# parameters
load_model_name = "model_ex5.h5"
load_model_path = 'data/models/'

num_of_examples = 363
dims = (500, 500)

x, y = dataset  .load_raw_train_with_cc(num_of_examples, dims)

model = load_model(load_model_path + load_model_name)

layer_name = 'fc4_conv2'
intermediate_layer_model = Model(inputs=model.input,
                                 outputs=model.get_layer(layer_name).output)
intermediate_output = intermediate_layer_model.predict(x)
output = model.predict(x)


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


def build_window(name, img, size=(500, 500), pos=(1, 1)):
    offset = (50, 50)
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, size[0], size[1])
    cv2.moveWindow(name, offset[0] + size[0] * pos[0], offset[1] + (size[1] + 30) * pos[1])
    cv2.imshow(name, img)


def move_next():
    cv2.waitKey()
    cv2.destroyAllWindows()


def transform_vector(vector):
    return np.array([vector[2], vector[1], vector[0]])


def correct_image(img, vector):
    img = img / vector[None, None, :]
    return img / img.max()


def rgb_bgr(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2BGR)


for i in range(0, num_of_examples):
    conf_map = intermediate_output[i, :, :, 3]
    conf_map = conf_map - conf_map.min()
    conf_map = conf_map / conf_map.max()
    img = rgb_bgr(x[i])
    corr_img = (correct_image(img, transform_vector(output[i])))
    gt_img = (correct_image(img, transform_vector(y[i])))
    window_name = "Confidence map " + str(i + 1)
    image_name = "Image " + str(i + 1)
    corr_name = "Corrected Image " + str(i + 1)
    gt_name = "GT Image " + str(i + 1)
    build_window(window_name, conf_map, dims, (0, 0))
    build_window(image_name, img, dims, (0, 1))
    build_window(corr_name, tone_map(corr_img), dims, (1, 0))
    build_window(gt_name, tone_map(gt_img), dims, (2, 0))
    move_next()
