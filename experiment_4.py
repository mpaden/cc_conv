from comet_ml import Experiment
import tensorflow.keras as tk
import sys
import dataset
import helpers
import errors
from model import build_VGG16_no_confidence
from model import load_model
from sklearn.utils import shuffle

experiment = Experiment(api_key="keK1E4PG0vGySUwP1w6RwCKMP", project_name="experiment_4", workspace="mpaden")

# parameters
resume = False
predictions_path = "predictions.txt"
save_model_name = "model_ex4.h5"
save_model_path = 'data/models/'
num_of_examples = 1707
dims = (500, 500)
batch_size = 16
epochs = 30
epochs_ft = 20
learning_rate = 0.0001
learning_rate_ft = 0.0001
validation_split = 0.15
test_split = 0.1
train_error = "mse"

optimizer = tk.optimizers.Adam(learning_rate=learning_rate)
optimizer_ft = tk.optimizers.Adam(learning_rate=learning_rate_ft)

# load data
x_train, y_train = dataset.load_raw_train(num_of_examples)
x_train, y_train = shuffle(x_train, y_train)

# init model
if resume:
    model = load_model(save_model_path + save_model_name)
else:
    model = build_VGG16_no_confidence()
    model.compile(optimizer, train_error, metrics=[errors.tf_mean_angular_error])

# do
try:
    model.fit(x_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs)
    helpers.unfreeze(model.layers[0])
    model.compile(optimizer_ft, train_error, metrics=[errors.tf_mean_angular_error])
    model.fit(x_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs_ft)
except KeyboardInterrupt:
    model.save(save_model_path + save_model_name, save_format='tf')
    print("Model saved as" + save_model_name)
    sys.exit()

# save model
model.save(save_model_path + save_model_name, save_format='tf')

# test
helpers.test_model(model, predictions_path)
