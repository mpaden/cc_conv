import matplotlib.pyplot as plt
import numpy as np
from tone_mapping import perform_flash
import dataset
from model import load_model

load_model_name = "model_ex1_cube.h5"
load_model_path = 'data/models/'
model = load_model(load_model_path + load_model_name)
dims = (500, 500)

x, y = dataset.load_test_display(50)


def correct_gray_world(img):
    mean_value = np.mean(img, axis=(0, 1))

    norm = np.linalg.norm(mean_value, 1)

    mean_value = mean_value / norm

    img = img / mean_value[None, None, :]
    img = (img / np.max(img))
    return img


def correct_gt(img, vector):
    img = img / vector[None, None, :]
    img = (img / np.max(img))
    return img


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


def correct_model(img):
    img_batch = np.expand_dims(img, 0)
    prediction = model.predict(img_batch)
    prediction = prediction[0]
    img = img / prediction[None, None, :]
    img = (img / np.max(img))
    return img


for i in range(0, y.shape[0]):
    f, axarr = plt.subplots(1, 3, figsize=(15,15))
    f.canvas.set_window_title("Image " + str(i+1))
    img = x[i]
    vector = y[i]

    axarr[0].imshow(tone_map(img))
    axarr[0].axis("off")
    axarr[0].text(0.5, -0.15, "a)", size=12, ha="center",
                  transform=axarr[0].transAxes)

    axarr[1].imshow(tone_map(correct_model(img)))
    axarr[1].axis("off")
    axarr[1].text(0.5, -0.15, "b)", size=12, ha="center",
                  transform=axarr[1].transAxes)

    axarr[2].imshow(tone_map(correct_gt(img, vector)))
    axarr[2].axis("off")
    axarr[2].text(0.5, -0.15, "c)", size=12, ha="center",
                  transform=axarr[2].transAxes)
    f.tight_layout()
    plt.show()
