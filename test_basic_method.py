import dataset
import numpy as np
from helpers import print_errors
import errors


def predict_gray_world(imgs):
    preds = []

    for img in imgs:
        non_black_pixels_mask = np.any(img != [0, 0, 0], axis=-1)
        mean_value = np.mean(img[non_black_pixels_mask], axis=0)
        norm = np.linalg.norm(mean_value, 1)
        preds.append(mean_value / norm)

    return np.array(preds)


def predict_white_patch(imgs):
    preds = []

    for img in imgs:
        mean_value = np.max(img, axis=(0, 1))
        norm = np.linalg.norm(mean_value, 1)
        preds.append(mean_value / norm)

    return np.array(preds)


def predict_do_nothing(imgs):
    num = 363
    array = []

    for i in range(0, num):
        array.append(np.array([1 / np.sqrt(3), 1 / np.sqrt(3), 1 / np.sqrt(3)]))

    return np.array(array)


method = predict_white_patch
# process

x_test, y_test = dataset.load_test()
predictions = method(x_test)
print("Angular errors: ")
print_errors(predictions, y_test, errors.angular_error)
print("Reproduction errors: ")
print_errors(predictions, y_test, errors.reproduction_error, error_name="reproduction")
