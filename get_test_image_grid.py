import matplotlib.pyplot as plt
import loader
import numpy as np
from tone_mapping import perform_flash

load_model_name = "model_ex1_best.h5"
load_model_path = 'data/models/'
img_path = 'data/spider_test_set/'
img_suffix = ".png"
num_of_examples = 363
dims = (1000, 1000)
imgs1 = []
imgs2 = []
imgs3 = []
img_nums1 = [150, 25, 362]
img_nums2 = [151, 27, 361]
img_nums3 = [157, 76, 98]


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


for img_num in img_nums1:
    imgs1.append(loader.load_image_spyder(img_path + str(img_num) + img_suffix, dims))

for img_num in img_nums2:
    imgs2.append(loader.load_image_spyder(img_path + str(img_num) + img_suffix, dims))

for img_num in img_nums3:
    imgs3.append(loader.load_image_spyder(img_path + str(img_num) + img_suffix, dims))

f, axarr = plt.subplots(len(imgs1), 3)
for i, img in enumerate(imgs1):
    num = img_nums1[i] - 1
    axarr[i, 0].imshow(img)
    axarr[i, 0].axis("off")


for i, img in enumerate(imgs2):
    num = img_nums2[i] - 1
    axarr[i, 1].imshow(img)
    axarr[i, 1].axis("off")

for i, img in enumerate(imgs3):
    num = img_nums3[i] - 1
    axarr[i, 2].imshow(img)
    axarr[i, 2].axis("off")

f.tight_layout()
plt.show()
