import numpy as np
import tensorflow as tf

def angular_error(r, gr):
    return np.arccos(np.dot(r, gr) / (
            np.power(r[0] ** 2 + r[1] ** 2 + r[2] ** 2, 1 / 2) * np.power(gr[0] ** 2 + gr[1] ** 2 + gr[2] ** 2,
                                                                          1 / 2)))


def reproduction_error(r, gr):
    dif = r / gr

    return np.arccos(np.dot(dif, np.ones(3)) / (
            np.power(dif[0] ** 2 + dif[1] ** 2 + dif[2] ** 2, 1 / 2) * np.power(3, 1 / 2)))


def tf_mean_angular_error(y_true, y_predicted):
    norm = ((tf.norm(y_true, axis=1, keepdims=True) * tf.norm(y_predicted, axis=1, keepdims=True)) + 1.e-8)
    out = tf.math.acos(tf.reduce_sum(tf.multiply(y_true, y_predicted), 1, keepdims=True) / norm)
    return tf.reduce_mean(out) * (180 / np.pi)
