import numpy as np
import matplotlib.pyplot as plt

import loader

# num_of_examples = 1707
label_path = "logs/predictions/predictions_ex1.txt"
# y_cube = loader.load_labels(label_path, num_of_examples)
# y_cube = y_cube[:, [0, 2]]

y_cube = loader.load_labels(label_path, 363)
y_cube = y_cube[:, [0, 2]]
img_path = 'data/spider_test_set/'
label_path = "data/spider_test_gt.txt"
num_of_examples = 363
y_spider = loader.load_labels(label_path, num_of_examples)
y_spider = y_spider[:, [0, 2]]

# Create plot
fig, ax = plt.subplots()

for dot in y_cube:
    x, y = dot[0], dot[1]
    ax.scatter(x, y, alpha=0.8, c="red", edgecolors='none', s=20)

for dot in y_spider:
    x, y = dot[0], dot[1]
    ax.scatter(x, y, alpha=0.8, c="blue", edgecolors='none', s=20)

plt.xlabel("$r$")
plt.ylabel("$b$")
plt.title('$rb$-chromaticities')
plt.show()
