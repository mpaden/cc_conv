from comet_ml import Experiment
import tensorflow.keras as tk
import sys
import dataset
import helpers
import errors
from model import load_model

experiment = Experiment(api_key="keK1E4PG0vGySUwP1w6RwCKMP", project_name="experiment_1_cube", workspace="mpaden")

# parameters
save_model_name = "model_ex1_cube.h5"
save_model_path = 'data/models/'
predictions_path = "predictions_ex1.txt"
num_of_examples = 1707
dims = (500, 500)
batch_size = 16
epochs_ft = 15
learning_rate_ft = 0.0001
validation_split = 0.1
test_split = 0.1
train_error = "mse"

optimizer_ft = tk.optimizers.Adam(learning_rate=learning_rate_ft)

# load data
X_train, y_train = dataset.load_raw_train(num_of_examples, dims)

# init model
model = load_model(save_model_path + save_model_name)

# do
try:
    helpers.unfreeze(model.layers[0])
    model.compile(optimizer_ft, train_error, metrics=[errors.tf_mean_angular_error])
    model.fit(X_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs_ft)
except KeyboardInterrupt:
    model.save(save_model_path + save_model_name, save_format='tf')
    print("Model saved as" + save_model_name)
    sys.exit()

# process
model.save(save_model_path + save_model_name, save_format='tf')
# test
helpers.test_model(model, predictions_path)
