import loader


def load_test(num_of_examples=363, dims=(500, 500)):
    img_path = 'data/spider_test_set/'
    img_suffix = ".png"
    label_path = "data/spider_test_gt.txt"
    x = loader.load_images_mp(img_path, img_suffix, num_of_examples, dims, loader.load_image_spyder)
    y = loader.load_labels(label_path, num_of_examples)

    return x, y


def load_test_display(num_of_examples=363, dims=(500, 500)):
    img_path = 'data/spider_test_set/'
    img_suffix = ".png"
    label_path = "data/spider_test_gt.txt"
    x = loader.load_images_mp(img_path, img_suffix, num_of_examples, dims, loader.load_image_spyder_display)
    y = loader.load_labels(label_path, num_of_examples)

    return x, y




def load_preprocessed_train(num_of_examples=1707, dims=(500, 500)):
    img_path = 'data/cube_plus_processed/'
    img_suffix = ".png"
    label_path = "data/cube+_gt.txt"
    x = loader.load_images_preprocessed(img_path, img_suffix, num_of_examples, dims)
    y = loader.load_labels(label_path, num_of_examples)

    return x, y


def load_validation_cube(num_of_examples=1707, dims=(500, 500)):
    img_path = 'data/cube_plus/'
    img_suffix = ".PNG"
    label_path = "data/cube+_gt.txt"
    x = loader.load_images_mp(img_path, img_suffix, num_of_examples, dims, loader.load_image_remove_cc)
    y = loader.load_labels(label_path, num_of_examples)

    return x[1550:], y[1550:]


def load_raw_train(num_of_examples=1707, dims=(500, 500)):
    img_path = 'data/cube_plus/'
    img_suffix = ".PNG"
    label_path = "data/cube+_gt.txt"
    x = loader.load_images_mp(img_path, img_suffix, num_of_examples, dims, loader.load_image_remove_cc)
    y = loader.load_labels(label_path, num_of_examples)

    return x, y


def load_raw_train_with_cc(num_of_examples=1707, dims=(500, 500)):
    img_path = 'data/cube_plus/'
    img_suffix = ".PNG"
    label_path = "data/cube+_gt.txt"
    x = loader.load_images_mp(img_path, img_suffix, num_of_examples, dims, loader.load_image_cube)
    y = loader.load_labels(label_path, num_of_examples)

    return x, y
