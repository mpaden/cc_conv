import helpers
from model import load_model

# parameters
load_model_name = "model_ex1_cube.h5"
load_model_path = "data/models/"
predictions_path = "predictions_ex1.txt"

# init model
model = load_model(load_model_path + load_model_name)

# process
helpers.test_model_validation(model, predictions_path)
