from comet_ml import Experiment
import tensorflow.keras as tk
import sys
import dataset
import helpers
import errors
from sklearn.utils import shuffle
from model import build_VGG16_basic
from model import load_model

experiment = Experiment(api_key="keK1E4PG0vGySUwP1w6RwCKMP", project_name="experiment_1", workspace="mpaden")

# parameters
resume = False
save_model_name = "model_ex1.h5"
save_model_path = 'data/models/'
predictions_path = "predictions_ex1.txt"
num_of_examples = 1707
dims = (500, 500)
batch_size = 16
epochs = 30
epochs_ft = 20
learning_rate = 0.0001
learning_rate_ft = 0.0001
validation_split = 0.1
test_split = 0.1
train_error = "mse"

optimizer = tk.optimizers.Adam(learning_rate=learning_rate)
optimizer_ft = tk.optimizers.Adam(learning_rate=learning_rate_ft)

# load data
X_train, y_train = dataset.load_raw_train(num_of_examples,dims)
X_train, y_train = shuffle(X_train, y_train)

# init model
if resume:
    model = load_model(save_model_path + save_model_name)
else:
    model = build_VGG16_basic()
    model.compile(optimizer, train_error, metrics=[errors.tf_mean_angular_error])

# do
try:
    model.fit(X_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs)
    helpers.unfreeze(model.layers[0])
    model.compile(optimizer_ft, train_error, metrics=[errors.tf_mean_angular_error])
    model.fit(X_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs_ft)
except KeyboardInterrupt:
    model.save(save_model_path + save_model_name, save_format='tf')
    print("Model saved as" + save_model_name)
    sys.exit()

# process
model.save(save_model_path + save_model_name, save_format='tf')
# test
helpers.test_model(model, predictions_path)
