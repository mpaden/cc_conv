import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras.models import Model
from errors import angular_error
from helpers import degree
from tone_mapping import perform_flash
import dataset
from model import load_model

load_model_name = "model_ex5.h5"
load_model_path = 'data/models/'
model = load_model(load_model_path + load_model_name)
dims = (500, 500)

x, y = dataset.load_raw_train_with_cc(300)
# GOOD
imgs = [
    313,
    324,
    334,
    39,
    44,
    251,
    96,
    276,
]

# BAD
imgs = [
    1,
    223,
    231,
    233,
    273,
    304,
]
# CC
imgs = [
    64,
    68,
    77,
    82,
    88,
    109,
]

layer_name = 'fc4_conv2'
intermediate_layer_model = Model(inputs=model.input,
                                 outputs=model.get_layer(layer_name).output)
intermediate_output = intermediate_layer_model.predict(x)


def correct_gray_world(img):
    mean_value = np.mean(img, axis=(0, 1))

    norm = np.linalg.norm(mean_value, 1)

    mean_value = mean_value / norm

    img = img / mean_value[None, None, :]
    img = (img / np.max(img))
    return img


def correct_gt(img, vector):
    img = img / vector[None, None, :]
    img = (img / np.max(img))
    return img


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


def correct_model(img):
    img_batch = np.expand_dims(img, 0)
    prediction = model.predict(img_batch)
    prediction = prediction[0]
    img = img / prediction[None, None, :]
    img = (img / np.max(img))
    return img, prediction


f, axarr = plt.subplots(len(imgs), 4)

for i, img_ind in enumerate(imgs, 0):
    ind = img_ind - 1

    vector = y[ind]
    img = x[ind]
    degree_sign = u'\N{DEGREE SIGN}'
    conf_map = intermediate_output[ind, :, :, 3]
    conf_map = conf_map / conf_map.max()

    corrected_image, pred = correct_model(img)

    axarr[i, 0].imshow(tone_map(img))
    axarr[i, 0].axis("off")

    axarr[i, 1].imshow(conf_map, cmap='gray')
    axarr[i, 1].axis("off")

    axarr[i, 2].imshow(tone_map(correct_gt(img, vector)))
    axarr[i, 2].axis("off")

    axarr[i, 3].imshow(tone_map(corrected_image))
    axarr[i, 3].axis("off")

    axarr[i, 3].text(1.2, 0.5, str(round(degree(angular_error(pred, vector)), 2)) + degree_sign, size=10, ha="center",
                     transform=axarr[i, 3].transAxes)

last_row = len(imgs) - 1
axarr[last_row, 0].text(0.5, -0.15, "a)", size=12, ha="center",
                        transform=axarr[last_row, 0].transAxes)
axarr[last_row, 1].text(0.5, -0.15, "b)", size=12, ha="center",
                        transform=axarr[last_row, 1].transAxes)
axarr[last_row, 2].text(0.5, -0.15, "c)", size=12, ha="center",
                        transform=axarr[last_row, 2].transAxes)
axarr[last_row, 3].text(0.5, -0.15, "d)", size=12, ha="center",
                        transform=axarr[last_row, 3].transAxes)

plt.show()
