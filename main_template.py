from comet_ml import Experiment
import tensorflow.keras as tk
import helpers
from model import build_VGG16_basic
from model import build_model_train_VGG16
from model import load_model
from sklearn.model_selection import train_test_split
import time

experiment = Experiment(api_key="keK1E4PG0vGySUwP1w6RwCKMP", project_name="general", workspace="mpaden")

start_time = time.time()

img_path = 'data/cube_plus_processed/'
img_suffix = ".png"
label_path = "data/cube+_gt.txt"
num_of_examples = 1707
dims = (500, 500)
batch_size = 8
epochs = 30
epochs_ft = 50
learning_rate = 0.01
learning_rate_ft = 0.01
validation_split = 0.1
test_split = 0.1
optimizer = tk.optimizers.SGD(learning_rate=learning_rate, momentum=0.0, nesterov=False)
optimizer_ft = tk.optimizers.SGD(learning_rate=learning_rate_ft, momentum=0.0, nesterov=False)

# load dataset
x = helpers.load_images_preprocessed(img_path, img_suffix, num_of_examples, dims)
y = helpers.load_labels(label_path, num_of_examples)
print("--- %s seconds ---" % (time.time() - start_time))
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=test_split)

# free memory
x = None
y = None
# init model
model = build_VGG16_basic()
model.compile(optimizer, "mse", metrics=[helpers.mean_angular_error])

# main fit loop
model.fit(X_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs)
helpers.unfreeze(model.layers[0])
model.compile(optimizer_ft, "mse", metrics=[helpers.mean_angular_error])
model.fit(X_train, y_train, batch_size=batch_size, validation_split=validation_split, epochs=epochs_ft)

model.save('data/models/model.h5', save_format='tf')
predictions_after = model.predict(X_test, batch_size=batch_size)
helpers.save_output("trained.txt", predictions_after)
print("After training: ")
helpers.print_errors(predictions_after, y_test)
