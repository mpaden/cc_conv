import tensorflow as tf
from tensorflow.keras.layers import Layer


class ConfidenceLayer(Layer):

    def call(self, inputs, **kwargs):
        pred = inputs[:, :, :, 0:3]
        conf = inputs[:, :, :, 3]
        conf = tf.expand_dims(conf, 3)
        conf = tf.tile(conf, [1, 1, 1, 3])

        return conf * pred

    def get_config(self):
        return super(ConfidenceLayer, self).get_config()


class SumLayerNorm(Layer):

    def call(self, inputs, **kwargs):
        shape = tf.shape(inputs)
        num = (tf.dtypes.cast(shape[1] * shape[2], tf.float32))
        out = (tf.math.reduce_sum(inputs, [1, 2]))
        div = tf.math.divide(out, num)
        norm = tf.math.reduce_sum(div, 1)
        norm = norm + 1.e-8
        norm = tf.expand_dims(norm, 1)
        norm = tf.tile(norm, [1, 3])

        return tf.math.divide(div, norm)

    def get_config(self):
        return super(SumLayerNorm, self).get_config()


class SumLayer(Layer):

    def call(self, inputs, **kwargs):
        shape = tf.shape(inputs)
        num = (tf.dtypes.cast(shape[1] * shape[2], tf.float32))
        out = (tf.math.reduce_sum(inputs, [1, 2]))
        div = tf.math.divide(out, num)
        return div

    def get_config(self):
        return super(SumLayer, self).get_config()
