import numpy as np
import cv2
import multiprocessing


def load_image_spyder(img_path, dims=(500, 500)):
    img = cv2.imread(img_path, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    black_level = 2048
    img = img.astype(np.int32, casting='safe')
    img = img - black_level
    img[img < 0] = 0
    img = img.astype(np.uint16)
    img = cv2.resize(img, dims)
    img = img / np.max(img)
    return img

def load_image_spyder_display(img_path, dims=(500, 500)):
    img = cv2.imread(img_path, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    black_level = 2048
    img = img.astype(np.int32, casting='safe')
    img = img - black_level
    img[img < 0] = 0
    mask_value = np.mean(img, axis=(0, 1))
    img = remove_cc2(img, mask_value, 900, 1900)
    img = img.astype(np.uint16)
    img = cv2.resize(img, dims)
    img = img / np.max(img)
    return img

def load_display_image(img_path, dims=(500, 500)):
    img = cv2.imread(img_path, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, dims)
    img = img / np.max(img)
    return img


def load_image_cube(img_path, dims=(500, 500)):
    img = cv2.imread(img_path, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    black_level = 2048
    img = img.astype(np.int32, casting='safe')
    img = img - black_level
    img[img < 0] = 0
    img = img.astype(np.uint16)
    img = cv2.resize(img, dims)
    img = img / np.max(img)
    return img


def load_image_remove_cc(img_path, dims=(500, 500)):
    img = cv2.imread(img_path, -1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    black_level = 2048
    img = img.astype(np.int32, casting='safe')
    img = img - black_level
    img[img < 0] = 0
    img = img.astype(np.uint16)
    mask_value = np.array([0, 0, 0])
    # mask_value = np.mean(img, axis=(0, 1))
    img = remove_cc2(img, mask_value)
    img = cv2.resize(img, dims)
    img = img / np.max(img)
    return img


def load_image_preprocessed(img_path):
    img = cv2.imread(img_path, -1)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img / np.max(img)


def remove_cc2(img, color_value, mask_x=1050, mask_y=2050):
    img[mask_x:, mask_y:] = color_value

    return img


def load_images_mp(path, suffix, num, dims=(500, 500), load_function=load_image_cube):
    processes = []
    queue = multiprocessing.Queue()
    images = np.zeros((num, dims[0], dims[1], 3), dtype=np.float32)
    todo = num
    num_of_threads = multiprocessing.cpu_count()
    batch_size = num // num_of_threads

    excess = num - num_of_threads * batch_size

    if excess > 0:
        start = num_of_threads * batch_size + 1
        stop = num_of_threads * batch_size + 1 + excess
        p = multiprocessing.Process(target=load_image_worker,
                                    args=(queue, start, stop, path, suffix, dims, load_function))
        processes.append(p)
        p.start()

    for i in range(0, num_of_threads):
        start = i * batch_size + 1
        stop = ((i + 1) * batch_size) + 1

        p = multiprocessing.Process(target=load_image_worker,
                                    args=(queue, start, stop, path, suffix, dims, load_function))
        processes.append(p)
        p.start()

    while todo > 0:
        i, img = queue.get(timeout=10)
        images[i - 1] = img
        todo -= 1

    for process in processes:
        process.join()

    return images


def load_images(path, suffix, num, dims=(500, 500)):
    images = []

    for i in range(1, num + 1):
        images.append(load_image_cube(path + str(i) + suffix, dims))
        print("Load image: " + str(i))

    return np.asarray(images)


def save_images(images):
    for i in range(1, len(images) + 1):
        cv2.imwrite("data/cube_plus_processed/" + str(i) + ".png", images[i - 1] * 255)


def load_image_worker(queue, start, stop, path, suffix, dims, load_function):
    for i in range(start, stop):
        queue.put((i, load_function(path + str(i) + suffix, dims)))
        print("Load image: " + str(i))


def load_and_save_images(path, suffix, num, dims=(500, 500), dest='data/cube_plus_processed/'):
    for i in range(1, num + 1):
        img = load_image_cube(path + str(i) + suffix, dims)
        print("Load image: " + str(i))
        cv2.imwrite(dest + str(i) + ".png", img)
        print("Save image: " + str(i))


def load_labels(label_path, num=1707):
    labels = []

    with open(label_path) as f:
        for i in range(1, num + 1):
            line = f.readline()
            points = line.split(" ", 3)
            labels.append(np.array([float(points[0]), float(points[1]), float(points[2])]))

    return np.asarray(labels)


def load_images_preprocessed(path, suffix, num, dims=(500, 500)):
    images = np.zeros((num, dims[0], dims[1], 3), dtype=np.float32)
    print("Loading images... ")
    for i in range(1, num + 1):
        images[i - 1] = load_image_preprocessed(path + str(i) + suffix)
    print("Images loaded.")
    return images
