import matplotlib.pyplot as plt
import loader
import numpy as np
from model import load_model
from tone_mapping import perform_flash

load_model_name = "model_ex1.h5"
load_model_path = 'data/models/'
dims = (500, 500)

i = 151

img_path = "data/cube_plus/" + str(i) + ".PNG"
img = loader.load_image_cube(img_path)


def correct_gray_world(img):
    mean_value = np.mean(img, axis=(0, 1))

    norm = np.linalg.norm(mean_value, 1)

    mean_value = mean_value / norm

    img = img / mean_value[None, None, :]
    img = (img / np.max(img))
    return img


def correct_gt(img):
    labels = loader.load_labels("data/cube+_gt.txt", 363)

    vector = labels[i - 1]

    img = img / vector[None, None, :]
    img = (img / np.max(img))
    return img


def correct_model(img):
    img_batch = np.expand_dims(img, 0)
    model = load_model(load_model_path + load_model_name)
    prediction = model.predict(img_batch)
    prediction = prediction[0]
    img = img / prediction[None, None, :]
    img = (img / np.max(img))
    return img


def tone_map(img):
    img = perform_flash(img)
    return img.astype(np.uint8)


f, axarr = plt.subplots(1, 2)

axarr[0].imshow(tone_map(img))
axarr[0].axis("off")

axarr[1].imshow(tone_map(correct_gt(img)))
axarr[1].axis("off")
f.tight_layout()
plt.show()
