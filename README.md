# Methods for Color Image Correction Based on Deep Learning

## Implementation Repository

This repository contains implementation code for final thesis on color constancy. The code contains model based on FC4 model for color constancy, implemented with VGG16 base network for feature extraction.
Model is implemented in TensorFlow 2.1 while images are imported with OpenCV. It also contains multiprocess implementation of image loading.